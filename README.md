# Backend

# REDU

REDU stands for Rekreasi Edukasi in Bahasa. REDU is a platform for people to share their experience on tourism, especially on its educational aspect. On top of that, the REDU team will also provide educational articles to enhance the atmosphere of the platform. The purpose of REDU is to increase people’s awareness of destinations that also offer education along with the recreation purpose.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
There are quite a few dependencies to be installed before we can start using this application, some of them are:
- express
- mongoose
- bcryptjs
- jsonwebtoken
- etc.

### Installing
All of package.json dependencies can be installed by typing in command below:
```
npm install
```
After installation is finished, start configuring the environment variable.
In this application, environment variable is saved to .env file. An example of .env file can be found on .env.example file.

## Running The Tests

To trigger unit testing code, simply type in command below:
```
npm test
```

For detailed script of test, see package.json scripts

## Running The Application on Local Machine

To fire up the server in local machine, simply type in one of two commands below:
```
npm start
```
or
```
npm run dev
```
npm start will fire up the server, but won't restart if there's any changes in code.
npm run dev uses nodemon, so it will monitor changes in code and attempt to restart the server.

For detailed script of dev, see package.json scripts

## Documentation

Documentation of how to use this application was done using swagger.
Documentation URL: [Documentation](https://ga5-redu-be.herokuapp.com/documentation/)

## Deployment

Deployment was done to herokuapp.
Deployment for development environment is done to heroku through gitlab-ci script

For detailed deployment script, see gitlag-ci.yml file

## Built With

* [Express](https://expressjs.com/) - Framework
* [MongoDB](https://www.mongodb.com/) - Database

## Authors
### GA#5 - Team E - REDU

* **Kevin Andrio** - Backend - *Initial work* - [ellvisca](https://gitlab.com/ellvisca)
* **Wahyu Narendra Kusumawardana.** - Backend - *Initial work* - [rendranarendra](https://gitlab.com/rendranarendra)
* **Pratur Anahata Sunaryo** - Frontend - *Initial work* - [pratur_hap](https://gitlab.com/pratur_hap)
* **Ernestia Manurung** - Frontend - *Initial work* - [ernesmanurung](https://gitlab.com/ernesmanurung)
* **Faidlur Rohman** - React Native - *Initial work* - [faidrohman](https://gitlab.com/faidrohman)



## Acknowledgements

* Special thanks to Fikri Rahmat Nurhidayat as authors' mentor - [FikriRNurhidayat](https://gitlab.com/FikriRNurhidayat) or [fnurhidayat](https://gitlab.com/)
* Special thanks to Isumi as authors' mentor - [isumizumi](https://gitlab.com/isumizumi)
* Special thanks to Yudi Krisnandi as authors' mentor - [yudi_kaka](https://gitlab.com/yudi_kaka)
* Hat tip to GA Batch#5 batchmates
