const supertest = require('supertest')
const mongoose = require('mongoose')
require('dotenv').config()

const server = require('../src/index.js')
const request = supertest(server)

const userFixtures = require('../src/fixtures/userFixture.js')
const staticAdmin = userFixtures.create()
const staticUser = userFixtures.create()

const articleFixtures = require('../src/fixtures/articleFixture.js')
const staticArticle = articleFixtures.create()

const storyFixtures = require('../src/fixtures/storyFixture.js')
const staticStory = storyFixtures.create()

const commentFixtures = require('../src/fixtures/commentFixture.js')
const staticComment = commentFixtures.create()

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        await collection.deleteMany()
    }
}

async function dropAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        try {
            await collection.drop()
        } catch (error) {
            if (error.message === 'ns not found') return
            if (error.message.includes('a background operation is currently running')) return
            console.log(error.message)
        }
    }
}

beforeAll(async (done) => {
    mongoose
        .connect(process.env.DB_CONNECTION_TEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        .then(() => {
            console.log('database connected')
        })
        .catch(err => console.error(err))
    await removeAllCollections()
    done()
})

afterAll(async (done) => {
    await dropAllCollections()
    await mongoose.connection.close()
    done()
})

describe('Create initial admin and article', () => {
    it('should create new admin', async done => {
        staticAdmin.password_confirmation = staticAdmin.password
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .post('/api/v1/articles')
                    .set('Content-Type', 'multipart/form-data')
                    .set('Authorization', 'Bearer ' + token)
                    .attach('image', `${__dirname}/../TestImage.jpg`)
                    .field('title', staticArticle.title)
                    .field('body', staticArticle.body)
                    .field('tags', staticArticle.tags)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })
})

describe('Create initial user and story', () => {
    it('should create new user', async done => {
        staticUser.password_confirmation = staticUser.password
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .post('/api/v1/story')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .field('title', staticStory.title)
                    .field('body', staticStory.body)
                    .field('category', staticStory.category)
                    .field('location', staticStory.location)
                    .field("long", staticStory.long)
                    .field("lat", staticStory.lat)
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Your story has been created')
                        expect(data).toHaveProperty('title', staticStory.title)
                        expect(data).toHaveProperty('body', staticStory.body)
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })
})

describe('POST /api/v1/comments', () => {
    it('should successfully post a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/articles')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        let articleComment = commentFixtures.create('Article')
                        request
                            .post('/api/v1/comments')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ originId: articleId })
                            .send(JSON.stringify(articleComment))
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully created comment")
                                expect(data).toHaveProperty('_id')
                                expect(data).toHaveProperty('message', articleComment.message)
                                expect(data).toHaveProperty('originId', articleId)
                                expect(data).toHaveProperty('createdAt')
                                done()
                            })
                    })
            })
    })

    it('should not successfully post a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/articles')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let articleId = 'articleId'
                        let articleComment = commentFixtures.create('Article')
                        request
                            .post('/api/v1/comments')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ originId: articleId })
                            .send(JSON.stringify(articleComment))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to create comment")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'ValidationError')
                                done()
                            })
                    })
            })
    })

    it('should successfully post a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/story/viewsAll')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let storyId = res.body.data.docs[i]._id
                        let storyComment = commentFixtures.create('Story')
                        request
                            .post('/api/v1/comments')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ originId: storyId })
                            .send(JSON.stringify(storyComment))
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully created comment")
                                expect(data).toHaveProperty('_id')
                                expect(data).toHaveProperty('message', storyComment.message)
                                expect(data).toHaveProperty('originId', storyId)
                                expect(data).toHaveProperty('createdAt')
                                done()
                            })
                    })
            })
    })

    it('should not successfully post a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/story/viewsAll')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let storyId = 'storyId'
                        let storyComment = commentFixtures.create('Story')
                        request
                            .post('/api/v1/comments')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ originId: storyId })
                            .send(JSON.stringify(storyComment))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to create comment")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'ValidationError')
                                done()
                            })
                    })
            })
    })
})

describe('GET /api/v1/comments/mine', () => {
    it("should show all user's comments", async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = Math.ceil(Math.random() * (lastPage))
                        request
                            .get('/api/v1/comments/mine')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ page: page})
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data} = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully showed comments")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })

    it("should show page 1 of all user's comments", async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = lastPage + 1
                        request
                            .get('/api/v1/comments/mine')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ page: page})
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data} = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully showed comments")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })

    it("should show page 1 of all user's comments on articles", async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Article',pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = lastPage + 1
                        request
                            .get('/api/v1/comments/mine')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ origin: 'Article',page: page})
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data} = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully showed comments")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })

    it("should show page 1 of all user's comments on stories", async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Story',pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = lastPage + 1
                        request
                            .get('/api/v1/comments/mine')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ origin: 'Story',page: page})
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data} = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully showed comments")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })
})

describe('PUT /api/v1/comments', () => {
    it('should successfully update a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Article', pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let commentId = res.body.data.docs[i]._id
                        let previousMessage = res.body.data.docs[i].message
                        let update = commentFixtures.create('Article')
                        request
                            .put('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .set('Content-Type', 'application/json')
                            .query({ commentId: commentId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe('Successfully edited comment')
                                expect(data).toHaveProperty('previousMessage', previousMessage)
                                expect(data).toHaveProperty('message', update.message)
                                expect(data).toHaveProperty('updatedAt')
                                done()
                            })
                    })
            })
    })

    it('should not successfully update a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Article', pagination: false })
                    .then(res => {
                        let commentId = 'commentId'
                        let update = commentFixtures.create('Article')
                        request
                            .put('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .set('Content-Type', 'application/json')
                            .query({ commentId: commentId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe('Failed to edit comment')
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should successfully update a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Story', pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let commentId = res.body.data.docs[i]._id
                        let previousMessage = res.body.data.docs[i].message
                        let update = commentFixtures.create('Story')
                        request
                            .put('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .set('Content-Type', 'application/json')
                            .query({ commentId: commentId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe('Successfully edited comment')
                                expect(data).toHaveProperty('previousMessage', previousMessage)
                                expect(data).toHaveProperty('message', update.message)
                                expect(data).toHaveProperty('updatedAt')
                                done()
                            })
                    })
            })
    })

    it('should not successfully update a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Story', pagination: false })
                    .then(res => {
                        let commentId = 'commentId'
                        let update = commentFixtures.create('Story')
                        request
                            .put('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .set('Content-Type', 'application/json')
                            .query({ commentId: commentId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe('Failed to edit comment')
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })
})

describe('DELETE /api/v1/comments', () => {
    it('not should successfully delete a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Article', pagination: false })
                    .then(res => {
                        let commentId = 'commentId'
                        request
                            .delete('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ commentId: commentId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to delete comment")
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should successfully delete a comment on an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Article', pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let commentId = res.body.data.docs[i]._id
                        request
                            .delete('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ commentId: commentId })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully deleted comment")
                                done()
                            })
                    })
            })
    })

    it('should not successfully delete a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Story', pagination: false })
                    .then(res => {
                        let commentId = 'commentId'
                        request
                            .delete('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ commentId: commentId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to delete comment")
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should successfully delete a comment on a story', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/comments/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ origin: 'Story', pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let commentId = res.body.data.docs[i]._id
                        request
                            .delete('/api/v1/comments')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ commentId: commentId })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully deleted comment")
                                done()
                            })
                    })
            })
    })
})
