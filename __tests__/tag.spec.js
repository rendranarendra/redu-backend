const supertest = require('supertest')
const mongoose = require('mongoose')
require('dotenv').config()

const server = require('../src/index.js')
const request = supertest(server)

const userFixtures = require('../src/fixtures/userFixture.js')
const staticAdmin = userFixtures.create()
const staticUser = userFixtures.create()

const articleFixtures = require('../src/fixtures/articleFixture.js')
const staticArticle = articleFixtures.create()

const tagFixtures = require('../src/fixtures/tagFixture.js')
const staticTag = tagFixtures.create()

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        await collection.deleteMany()
    }
}

async function dropAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        try {
            await collection.drop()
        } catch (error) {
            if (error.message === 'ns not found') return
            if (error.message.includes('a background operation is currently running')) return
            console.log(error.message)
        }
    }
}

beforeAll(async (done) => {
    mongoose
        .connect(process.env.DB_CONNECTION_TEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        .then(() => {
            console.log('database connected')
        })
        .catch(err => console.error(err))
    await removeAllCollections()
    done()
})

afterAll(async (done) => {
    await dropAllCollections()
    await mongoose.connection.close()
    done()
})

describe('Create initial user', () => {
    it('should create new user', async done => {
        staticUser.password_confirmation = staticUser.password
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Your account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                done()
            })
    })
})

describe('Create initial admin', () => {
    it('should create new admin', async done => {
        staticAdmin.password_confirmation = staticAdmin.password
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Admin account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                expect(data).toHaveProperty('role', 'ADMIN')
                done()
            })
    })
})

describe('Create initial article', () => {
    it('should create a new article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .post('/api/v1/articles')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .send(JSON.stringify(staticArticle))
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })
})

describe('POST /api/v1/tags', () => {
    it('should create a new tag', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .post('/api/v1/tags')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .send(JSON.stringify(staticTag))
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe("Successfully created tag")
                        expect(data).toHaveProperty('_id')
                        expect(data).toHaveProperty('name', staticTag.name)
                        done()
                    })
            })
    })

    it('should not create a new tag due to missing information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let tagSample = tagFixtures.create()
                delete tagSample.name
                request
                    .post('/api/v1/tags')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .send(JSON.stringify(tagSample))
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe("Failed to create tag")
                        expect(data).toBe(null)
                        expect(errors).toHaveProperty('name', 'ValidationError')
                        done()
                    })
            })
    })

    it('should not create a new tag because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let tagSample = tagFixtures.create()
                request
                    .post('/api/v1/tags')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .send(JSON.stringify(tagSample))
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe("Failed to create tag")
                        expect(data).toBe(null)
                        expect(errors).toBe("You're not allowed to add tag")
                        done()
                    })
            })
    })
})


describe('PUT /api/v1/tags', () => {
    it('should update a tag', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.length - 1))
                        let tag = res.body.data[i]
                        let tagId = tag._id
                        let update = tagFixtures.create()
                        request
                            .put('/api/v1/tags')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully updated tag")
                                expect(data).toHaveProperty('_id', tagId)
                                expect(data).toHaveProperty('previousName', tag.name)
                                expect(data).toHaveProperty('name', update.name)
                                expect(data).toHaveProperty('updatedAt')
                                done()
                            })
                    })
            })
    })

    it('should not update a tag due to invalid tagId', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let tagId = 'tagId'
                        let update = tagFixtures.create()
                        request
                            .put('/api/v1/tags')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to update tag")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should not update a tag because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.length - 1))
                        let tag = res.body.data[i]
                        let tagId = tag._id
                        let update = tagFixtures.create()
                        request
                            .put('/api/v1/tags')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .send(JSON.stringify(update))
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to update tag")
                                expect(data).toBe(null)
                                expect(errors).toBe("You're not allowed to update tag")
                                done()
                            })
                    })
            })
    })
})

describe('DELETE /api/v1/tags', () => {
    it('should not delete a tag because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.length - 1))
                        let tag = res.body.data[i]
                        let tagId = tag._id
                        request
                            .delete('/api/v1/tags')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to delete tag")
                                expect(data).toBe(null)
                                expect(errors).toBe("You're not allowed to delete tag")
                                done()
                            })
                    })
            })
    })

    it('should not delete a tag due to invalid tagId', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let tagId = 'tagId'
                        request
                            .delete('/api/v1/tags')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to delete tag")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should delete a tag', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/tags')
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.length - 1))
                        let tag = res.body.data[i]
                        let tagId = tag._id
                        request
                            .delete('/api/v1/tags')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ tagId: tagId })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully deleted tag")
                                expect(data).toHaveProperty('_id', tagId)
                                expect(data).toHaveProperty('name', tag.name)
                                expect(data).toHaveProperty('status', 'Deleted')
                                done()
                            })
                    })
            })
    })
})
