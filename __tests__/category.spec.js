const supertest = require("supertest");
const mongoose = require("mongoose");
require("dotenv").config();

const server = require("../src/index.js");
const request = supertest(server);

const userFixtures = require("../src/fixtures/userFixture.js");
const staticAdmin = userFixtures.create();
const staticUser = userFixtures.create();

const categoryFixtures = require("../src/fixtures/categoryFixture.js");
const staticCategory = categoryFixtures.create();

async function removeAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    await collection.deleteMany();
  }
}

async function dropAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    try {
      await collection.drop();
    } catch (error) {
      if (error.message === "ns not found") return;
      if (error.message.includes("a background operation is currently running"))
        return;
      console.log(error.message);
    }
  }
}

beforeAll(async done => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
      console.log("database connected");
    })
    .catch(err => console.error(err));
  await removeAllCollections();
  done();
});

afterAll(async done => {
  await dropAllCollections();
  await mongoose.connection.close();
  done();
});

describe("Create initial admin", () => {
  it("should create new admin", async done => {
    staticAdmin.password_confirmation = staticAdmin.password;
    request
      .post("/api/v1/admin")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        expect(res.status).toBe(201);
        let { status, message, data } = res.body;
        expect(status).toBe(true);
        expect(message).toBe("Admin account has been created");
        expect(data).toHaveProperty("name");
        expect(data).toHaveProperty("username");
        expect(data).toHaveProperty("email");
        expect(data).toHaveProperty("token");
        expect(data).toHaveProperty("role", "ADMIN");
        done();
      });
  });
});

describe("Create initial user", () => {
  it("should create new user", async done => {
    staticUser.password_confirmation = staticUser.password;
    request
      .post("/api/v1/users")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        expect(res.status).toBe(201);
        let { status, message, data } = res.body;
        expect(status).toBe(true);
        expect(message).toBe("Your account has been created");
        expect(data).toHaveProperty("name");
        expect(data).toHaveProperty("username");
        expect(data).toHaveProperty("email");
        expect(data).toHaveProperty("token");
        done();
      });
  });
});

describe("Create new category", () => {
  it("should create a new category", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        let token = res.body.data.token;
        let id = res.body.data._id;
        request
          .post("/api/v1/category/create")
          .set("Content-Type", "application/json")
          .set("Authorization", "Bearer " + token)
          .send(JSON.stringify(staticCategory))
          .then(res => {
            expect(res.status).toBe(200);
            let { status, message, data } = res.body;
            expect(status).toBe(true);
            expect(message).toBe("Successfully add new category");
            expect(data).toHaveProperty("_id");
            done();
          });
      });
  });

  it("should not create a new category because user is not admin", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        let token = res.body.data.token;
        let categorySample = categoryFixtures.create();
        request
          .post("/api/v1/category/create")
          .set("Content-Type", "application/json")
          .set("Authorization", "Bearer " + token)
          .send(JSON.stringify(categorySample))
          .then(res => {
            expect(res.status).toBe(422);
            let { status, message, data, errors } = res.body;
            expect(status).toBe(false);
            expect(message).toBe("Failed to add new category");
            expect(data).toBe(null);
            expect(errors).toBe("You're not allowed to create category");
            done();
          });
      });
  });
});

describe("PUT /api/v1/category", () => {
  it("should update a category", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category").then(res => {
          let i = Math.floor(Math.random() * (res.body.data.length - 1));
          let category = res.body.data[i];
          let categoryId = category._id;
          let update = categoryFixtures.create();
          request
            .put("/api/v1/category/update")
            .set("Content-Type", "application/json")
            .set("Authorization", "Bearer " + token)
            .query({ id: categoryId })
            .send(JSON.stringify(update))
            .then(res => {
              expect(res.status).toBe(200);
              let { status, message, data } = res.body;
              expect(status).toBe(true);
              expect(message).toBe("Successfully update a category");
              expect(data).toHaveProperty("_id", categoryId);
              done();
            });
        });
      });
  });

  it("should not update a category due to invalid categoryId", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category/update").then(res => {
          let categoryId = "categoryId";
          let update = categoryFixtures.create();
          request
            .put("/api/v1/category/update")
            .set("Content-Type", "application/json")
            .set("Authorization", "Bearer " + token)
            .query({ id: categoryId + "error lah" })
            .send(JSON.stringify(update))
            .then(res => {
              expect(res.status).toBe(422);
              let { status, message, data } = res.body;
              expect(status).toBe(false);
              expect(message).toBe("Failed to update category");
              expect(data).toBe(null);
              done();
            });
        });
      });
  });

  it("should not update a category because user is not admin", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category").then(res => {
          let i = Math.floor(Math.random() * (res.body.data.length - 1));
          let category = res.body.data[i];
          let categoryId = category._id;
          let update = categoryFixtures.create();
          request
            .put("/api/v1/category/update")
            .set("Content-Type", "application/json")
            .set("Authorization", "Bearer " + token)
            .query({ categoryId: categoryId })
            .send(JSON.stringify(update))
            .then(res => {
              expect(res.status).toBe(422);
              let { status, message, data, errors } = res.body;
              expect(status).toBe(false);
              expect(message).toBe("Failed to update category");
              expect(data).toBe(null);
              expect(errors).toBe("You're not allowed to update category");
              done();
            });
        });
      });
  });
});

describe("DELETE /api/v1/category", () => {
  it("should not delete a category because user is not admin", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category").then(res => {
          let i = Math.floor(Math.random() * (res.body.data.length - 1));
          let category = res.body.data[i];
          let categoryId = category._id;
          request
            .delete("/api/v1/category/delete")
            .set("Authorization", "Bearer " + token)
            .query({ id: categoryId })
            .then(res => {
              expect(res.status).toBe(422);
              let { status, message, data, errors } = res.body;
              expect(status).toBe(false);
              expect(message).toBe("Failed to delete selected category");
              expect(data).toBe(null);
              expect(errors).toBe("You're not allowed to delete category");
              done();
            });
        });
      });
  });

  it("should not delete a tag due to invalid categoryId", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category").then(res => {
          let categoryId = "categoryId";
          request
            .delete("/api/v1/category/delete")
            .set("Authorization", "Bearer " + token)
            .query({ id: categoryId + "error test" })
            .then(res => {
              expect(res.status).toBe(422);
              let { status, message, data, errors } = res.body;
              expect(status).toBe(false);
              expect(message).toBe("Failed to delete selected category");
              expect(data).toBe(null);
              expect(errors).toHaveProperty("name", "CastError");
              done();
            });
        });
      });
  });

  it("should delete a category", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticAdmin))
      .then(res => {
        let token = res.body.data.token;
        request.get("/api/v1/category").then(res => {
          let i = Math.floor(Math.random() * (res.body.data.length - 1));
          let category = res.body.data[i];
          let categoryId = category._id;
          request
            .delete("/api/v1/category/delete")
            .set("Authorization", "Bearer " + token)
            .query({ _id: categoryId })
            .then(res => {
              expect(res.status).toBe(200);
              let { status, message, data } = res.body;
              expect(status).toBe(true);
              expect(message).toBe("Successfully delete selected category");
              expect(data).toHaveProperty("_id", categoryId);
              expect(data).toHaveProperty("name", category.name);
              done();
            });
        });
      });
  });
});
