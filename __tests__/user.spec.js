const supertest = require('supertest')
const mongoose = require('mongoose')
require('dotenv').config()

const server = require('../src/index.js')
const request = supertest(server)

const userFixtures = require('../src/fixtures/userFixture.js')
const staticUser = userFixtures.create()
const staticAdmin = userFixtures.create()

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        await collection.deleteMany()
    }
}

async function dropAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        try {
            await collection.drop()
        } catch (error) {
            if (error.message === 'ns not found') return
            if (error.message.includes('a background operation is currently running')) return
            console.log(error.message)
        }
    }
}

beforeAll(async (done) => {
    mongoose
        .connect(process.env.DB_CONNECTION_TEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        .then(() => {
            console.log('database connected')
        })
        .catch(err => console.error(err))
    await removeAllCollections()
    done()
})

afterAll(async (done) => {
    await dropAllCollections()
    await mongoose.connection.close()
    done()
})


describe('POST /api/v1/users', () => {
    it('should create new user', async done => {
        staticUser.password_confirmation = staticUser.password
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Your account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                done()
            })
    })

    it('should not create new user due to password and confirmation mismatch', async done => {
        let userSample = userFixtures.create()
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(userSample))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe('A problem has been encountered while creating your account')
                expect(data).toBe(null)
                expect(errors).toBe("Password confirmation does not match!")
                done()
            })
    })

    it('should not create new user due to registered email', async done => {
        let userSample = {
            ...staticUser
        }
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(userSample))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe('A problem has been encountered while creating your account')
                expect(data).toBe(null)
                expect(errors).toHaveProperty('name', 'MongoError')
                done()
            })
    })
})

describe('POST /api/v1/auth', () => {
    it('should log user in', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                expect(res.status).toBe(200)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("You've successfully logged in")
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('token')
                done()
            })
    })

    it('should log user in because user is not registered', async done => {
        let invalidUser = {
            username: 'invalid',
            password: 'invalid'
        }
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(invalidUser))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe("Login failed")
                expect(data).toBe(null)
                expect(errors).toBe('User not found!')
                done()
            })
    })

    it('should not log user in', async done => {
        let userSample = {
            ...staticUser,
            password: ""
        }
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(userSample))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe('Login failed')
                expect(data).toBe(null)
                expect(errors).toBe('Incorrect Password!')
                done()
            })
    })
})

describe('GET /api/v1/users', () => {
    it('should get current user information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/users')
                    .set('Authorization', 'Bearer ' + token)
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Current user information: ')
                        expect(data).toHaveProperty('name')
                        expect(data).toHaveProperty('username')
                        expect(data).toHaveProperty('email')
                        done()
                    })
            })
    })

    it('should not get current user information due to invalid token', async done => {
        request
            .get('/api/v1/users')
            .set('Authorization', "")
            .then(res => {
                expect(res.status).toBe(401)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe('Invalid Token')
                expect(data).toBe(null)
                expect(errors).toHaveProperty('name', 'JsonWebTokenError')
                done()
            })
    })
})

describe('PUT /api/v1/users', () => {
    it('should update profile information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let update = userFixtures.create()
                request
                    .put('/api/v1/users')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .field('name', update.name)
                    .field('bio', update.bio)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Your profile has been successfully updated')
                        expect(data).toHaveProperty('name')
                        expect(data).toHaveProperty('username')
                        expect(data).toHaveProperty('email')
                        done()
                    })
            })
    })

    it('should update profile picture information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                let update = userFixtures.create()
                request
                    .put('/api/v1/users')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .attach('image', `${__dirname}/../TestProfile.jpg`)
                    .field('bio', update.bio)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Your profile has been successfully updated')
                        expect(data).toHaveProperty('name')
                        expect(data).toHaveProperty('username')
                        expect(data).toHaveProperty('email')
                        done()
                    })
            })
    })
})

describe('DELETE /api/v1/users', () => {
    it('should deactivate current user', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .delete('/api/v1/users')
                    .set('Authorization', 'Bearer ' + token)
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Your account has been deactivated')
                        expect(data).toHaveProperty('isActive', false)
                        done()
                    })
            })
    })
})

describe('POST /api/v1/admin', () => {
    it('should create new admin', async done => {
        staticAdmin.password_confirmation = staticAdmin.password
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Admin account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                expect(data).toHaveProperty('role', 'ADMIN')
                done()
            })
    })

    it('should not create new admin due to confirmation does not match', async done => {
        let adminSample = userFixtures.create()
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(adminSample))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe("A problem has been encountered while creating admin account")
                expect(data).toBe(null)
                expect(errors).toBe("Password confirmation does not match!")
                done()
            })
    })

    it('should not create new admin due to duplicate email', async done => {
        let adminSample = {
            ...staticAdmin
        }
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(adminSample))
            .then(res => {
                expect(res.status).toBe(422)
                let { status, message, data, errors } = res.body
                expect(status).toBe(false)
                expect(message).toBe("A problem has been encountered while creating admin account")
                expect(data).toBe(null)
                expect(errors).toHaveProperty('name', 'MongoError')
                done()
            })
    })
})