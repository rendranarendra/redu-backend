const supertest = require("supertest");
const mongoose = require("mongoose");
require("dotenv").config();

const server = require("../src/index.js");
const request = supertest(server);

const { create: generateUser } = require("../src/fixtures/userFixture.js");

const story = require("../src/models/story.js");
const storyFixtures = require("../src/fixtures/storyFixture.js");
const staticStory = storyFixtures.create();

const User = require("../src/models/user.js");
const user = generateUser();
const userFixtures = require("../src/fixtures/userFixture.js");
const staticUser = userFixtures.create();

async function removeAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    await collection.deleteMany();
  }
}

async function dropAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    try {
      await collection.drop();
    } catch (error) {
      if (error.message === "ns not found") return;
      if (error.message.includes("a background operation is currently running"))
        return;
      console.log(error.message);
    }
  }
}

beforeAll(async done => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
      console.log("database connected");
    })
    .catch(err => console.error(err));
  await removeAllCollections();
  done();
});

afterAll(async done => {
  await dropAllCollections();
  await mongoose.connection.close();
  done();
});

describe("POST /api/v1/users", () => {
  it("should create new user", async done => {
    staticUser.password_confirmation = staticUser.password;
    request
      .post("/api/v1/users")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        expect(res.status).toBe(201);
        let { status, message, data } = res.body;
        expect(status).toBe(true);
        expect(message).toBe("Your account has been created");
        done();
      });
  });

  describe("Story endpoint", () => {
    it("create a new story", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        request
          .post("/api/v1/story")
          .set("Content-Type", "mulipart/form-data")
          .set("Authorization", "Bearer " + data.token)
          .attach("image", `${__dirname}/../TestImage.jpg`)
          .field("title", staticStory.title)
          .field("body", staticStory.body)
          .field("category", staticStory.category)
          .field("location", staticStory.location)
          .field("long", staticStory.long)
          .field("lat", staticStory.lat)
          .then(res => {
            const { status, data } = res.body;
            expect(status).toBe(true);
            expect(res.statusCode).toEqual(200);
            expect(typeof data).toEqual("object");
            expect(true).toEqual(true);
            done();
          });
      });
    });
  });

  describe("Story endpoint", () => {
    it("Should not create a new story due to missing required field", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        request
          .post("/api/v1/story")
          .set("Content-Type", "mulipart/form-data")
          .set("Authorization", "Bearer " + data.token)
          .field("body", staticStory.body)
          .field("category", staticStory.category)
          .field("location", staticStory.location)
          .then(res => {
            const { status, data } = res.body;
            expect(status).toBe(false);
            expect(res.statusCode).toEqual(422);
            expect(typeof data).toEqual("object");
            done();
          });
      });
    });
  });

  describe("Story endpoint", () => {
    it("Should return all user's stories and paginate it", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + data.token)
          .then(res => {
            const { status, data } = res.body;
            expect(status).toBe(true);
            expect(res.statusCode).toEqual(200);
            expect(typeof data).toEqual("object");
            expect(true).toEqual(true);
            done();
          });
      });
    });
  });

  describe("Story endpoint", () => {
    it("Should sort user story by param", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let sortParam = ["title", "createdAt", "category", "location"];
            let randomSort =
              sortParam[Math.floor(Math.random() * sortParam.length)];
            let lastPage = Math.ceil(res.body.data.totalDocs / 10);
            let page = Math.ceil(Math.random() * lastPage);
            request
              .get("/api/v1/story")
              .set("Authorization", "Bearer " + token)
              .query({ sort: randomSort, order: "ascending", page: page })
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(true);
                expect(res.statusCode).toEqual(200);
                expect(typeof data).toEqual("object");
                expect(true).toEqual(true);
                done();
              });
          });
      });
    });

    it("Should sort user story by param descending order", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let sortParam = ["title", "createdAt", "category", "location"];
            let randomSort =
              sortParam[Math.floor(Math.random() * sortParam.length)];
            let lastPage = Math.ceil(res.body.data.totalDocs / 10);
            let page = Math.ceil(Math.random() * lastPage);
            request
              .get("/api/v1/story")
              .set("Authorization", "Bearer " + token)
              .query({ sort: randomSort, order: "descending", page: page })
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(true);
                expect(res.statusCode).toEqual(200);
                expect(typeof data).toEqual("object");
                expect(true).toEqual(true);
                done();
              });
          });
      });
    });

    it("Should fail sort user story by param", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let sortParam = ["error"];
            let randomSort =
              sortParam[Math.floor(Math.random() * sortParam.length)];
            let lastPage = Math.ceil(res.body.data.totalDocs / 10);
            let page = Math.ceil(Math.random() * lastPage);
            request
              .get("/api/v1/story")
              .set("Authorization", "Bearer " + token)
              .query({ sort: randomSort, order: "ascending", page: page })
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(false);
                expect(res.statusCode).toEqual(422);
                expect(typeof data).toEqual("object");
                expect(false).toEqual(false);
                done();
              });
          });
      });
    });

    it("Should fail sort user story by param descending", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let sortParam = ["error"];
            let randomSort =
              sortParam[Math.floor(Math.random() * sortParam.length)];
            let lastPage = Math.ceil(res.body.data.totalDocs / 10);
            let page = Math.ceil(Math.random() * lastPage);
            request
              .get("/api/v1/story")
              .set("Authorization", "Bearer " + token)
              .query({ sort: randomSort, order: "descending", page: page })
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(false);
                expect(res.statusCode).toEqual(422);
                expect(typeof data).toEqual("object");
                expect(false).toEqual(false);
                done();
              });
          });
      });
    });
  });

  it("Should fail sort user's story due to invalid sorting order", done => {
    User.login({
      username: staticUser.username,
      password: staticUser.password
    }).then(data => {
      let token = data.token;
      request
        .get("/api/v1/story/views")
        .query({ page: 1 })
        .set("Authorization", "Bearer " + token)
        .then(res => {
          let sortParam = ["title", "createdAt", "category", "location"];
          let randomSort =
            sortParam[Math.floor(Math.random() * sortParam.length)];
          let lastPage = Math.ceil(res.body.data.totalDocs / 10);
          let page = Math.ceil(Math.random() * lastPage);
          request
            .get("/api/v1/story")
            .set("Authorization", "Bearer " + token)
            .query({ sort: randomSort, order: "error", page: page })
            .then(res => {
              const { status, data } = res.body;
              expect(status).toBe(false);
              expect(res.statusCode).toEqual(422);
              expect(typeof data).toEqual("object");
              expect(false).toEqual(false);
              done();
            });
        });
    });
  });

  describe("Story endpoint", () => {
    it("Should update user story by id", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
            let storyId = res.body.data.docs[i]._id;
            request
              .put("/api/v1/story/update")
              .query({ id: storyId })
              .set("Authorization", "Bearer " + token)
              .attach("image", `${__dirname}/../TestImage.jpg`)
              .field("body", staticStory.body)
              .field("category", staticStory.category)
              .field("location", staticStory.location)
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(true);
                expect(res.statusCode).toEqual(201);
                expect(typeof data).toEqual("object");
                expect(true).toEqual(true);
                done();
              });
          });
      });
    });
  });

  describe("Story endpoint", () => {
    it("Should update user story by id without image input", done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
            let storyId = res.body.data.docs[i]._id;
            request
              .put("/api/v1/story/update")
              .query({ id: storyId })
              .set("Authorization", "Bearer " + token)
              .field("body", staticStory.body)
              .field("category", staticStory.category)
              .field("location", staticStory.location)
              .then(res => {
                const { status, data } = res.body;
                expect(status).toBe(true);
                expect(res.statusCode).toEqual(201);
                expect(typeof data).toEqual("object");
                expect(true).toEqual(true);
                done();
              });
          });
      });
    });
  });

  describe("GET /api/v1/story/get", () => {
    it("should show one specific story", async done => {
      request
        .get("/api/v1/story/viewsAll")
        .query({ pagination: false })
        .then(res => {
          let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
          let story = res.body.data.docs[i];
          let storyId = story._id;
          request
            .get("/api/v1/story/get")
            .query({ id: storyId })
            .then(res => {
              expect(res.status).toBe(200);
              let { status, message, data } = res.body;
              expect(status).toBe(true);
              expect(message).toBe("Successfully get selected story");
              expect(data).toHaveProperty("title", story.body.title);
              expect(data).toHaveProperty("body", story.body.body);
              done();
            });
        });
    });

    it("should not show one specific story due to invalid id", async done => {
      request
        .get("/api/v1/story/viewsAll")
        .query({ pagination: false })
        .then(res => {
          let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
          let story = res.body.data.docs[i];
          let storyId = story._id;
          request
            .get("/api/v1/story/get")
            .query({ id: storyId + "error lah" })
            .then(res => {
              expect(res.status).toBe(404);
              let { status, message, data } = res.body;
              expect(status).toBe(false);
              expect(message).toBe("Story not found!");
              done();
            });
        });
    });
  });

  describe("POST /api/v1/story/like", () => {
    it("should successfully like a story", async done => {
      User.login({
        username: staticUser.username,
        password: staticUser.password
      }).then(data => {
        let token = data.token;
        request
          .get("/api/v1/story/views")
          .query({ page: 1 })
          .set("Authorization", "Bearer " + token)
          .then(res => {
            let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
            let story = res.body.data.docs[i];
            let storyId = story._id;
            request
              .post("/api/v1/story/like")
              .query({ storyId: storyId })
              .set("Authorization", "Bearer " + token)
              .then(res => {
                expect(res.status).toBe(201);
                let { status, message, data } = res.body;
                expect(status).toBe(true);
                expect(message).toBe("Successfully liked story");
                expect(data).toHaveProperty("_id", storyId);
                expect(data).toHaveProperty("title", story.title);
                expect(data).toHaveProperty("likes");
                done();
              });
          });
      });
    });
  });

  it("should not successfully like a story because user previously liked the story", async done => {
    request
      .post("/api/v1/auth")
      .set("Content-Type", "application/json")
      .send(JSON.stringify(staticUser))
      .then(res => {
        let token = res.body.data.token;
        let id = res.body.data._id;
        let storySample = storyFixtures.create();
        request
          .post("/api/v1/story")
          .set("Content-Type", "mulipart/form-data")
          .set("Authorization", "Bearer " + token)
          .attach("image", `${__dirname}/../TestImage.jpg`)
          .field("title", storySample.title)
          .field("body", storySample.body)
          .field("category", storySample.category)
          .field("location", storySample.location)
          .field("long", storySample.long)
          .field("lat", storySample.lat)
          .then(res => {
            let storyId = res.body.data._id;
            request
              .post("/api/v1/story/like")
              .set("Authorization", "Bearer " + token)
              .query({ storyId: storyId })
              .then(res => {
                request
                  .post("/api/v1/story/like")
                  .set("Authorization", "Bearer " + token)
                  .query({ storyId: storyId })
                  .then(res => {
                    expect(res.status).toBe(422);
                    let { status, message, data, errors } = res.body;
                    expect(status).toBe(false);
                    expect(message).toBe("Failed to like story");
                    expect(data).toBe(null);
                    expect(errors).toBe("You've already liked this story");
                    done();
                  });
              });
          });
      });
  });
});

describe("Story endpoint", () => {
  it("Should not be able to delete user story due to missing id", done => {
    User.login({
      username: staticUser.username,
      password: staticUser.password
    }).then(data => {
      let token = data.token;
      request
        .get("/api/v1/story/views")
        .query({ page: 1 })
        .set("Authorization", "Bearer " + token)
        .then(res => {
          let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
          let storyId = res.body.data.docs[i]._id;
          request
            .delete("/api/v1/story/delete")
            .query({ id: storyId + "error" })
            .set("Authorization", "Bearer " + token)
            .then(res => {
              const { status, data } = res.body;
              expect(status).toBe(false);
              expect(res.statusCode).toEqual(422);
              expect(typeof data).toEqual("object");
              done();
            });
        });
    });
  });
});

describe("Story endpoint", () => {
  it("Should delete user story by id", done => {
    User.login({
      username: staticUser.username,
      password: staticUser.password
    }).then(data => {
      let token = data.token;
      request
        .get("/api/v1/story/views")
        .query({ page: 1 })
        .set("Authorization", "Bearer " + token)
        .then(res => {
          let i = Math.floor(Math.random() * (res.body.data.docs.length - 1));
          let storyId = res.body.data.docs[i]._id;
          request
            .delete("/api/v1/story/delete")
            .query({ id: storyId })
            .set("Authorization", "Bearer " + token)
            .then(res => {
              const { status, data } = res.body;
              expect(status).toBe(true);
              expect(res.statusCode).toEqual(200);
              expect(typeof data).toEqual("string");
              expect(true).toEqual(true);
              done();
            });
        });
    });
  });
});
