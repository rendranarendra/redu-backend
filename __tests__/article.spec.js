const supertest = require('supertest')
const mongoose = require('mongoose')
require('dotenv').config()

const server = require('../src/index.js')
const request = supertest(server)

const userFixtures = require('../src/fixtures/userFixture.js')
const staticAdmin = userFixtures.create()
const staticUser = userFixtures.create()

const articleFixtures = require('../src/fixtures/articleFixture.js')
const staticArticle = articleFixtures.create()
const staticArticle2 = articleFixtures.create()

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        await collection.deleteMany()
    }
}

async function dropAllCollections() {
    const collections = Object.keys(mongoose.connection.collections)
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName]
        try {
            await collection.drop()
        } catch (error) {
            if (error.message === 'ns not found') return
            if (error.message.includes('a background operation is currently running')) return
            console.log(error.message)
        }
    }
}

beforeAll(async (done) => {
    mongoose
        .connect(process.env.DB_CONNECTION_TEST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        .then(() => {
            console.log('database connected')
        })
        .catch(err => console.error(err))
    await removeAllCollections()
    done()
})

afterAll(async (done) => {
    await dropAllCollections()
    await mongoose.connection.close()
    done()
})

describe('Create initial admin', () => {
    it('should create new admin', async done => {
        staticAdmin.password_confirmation = staticAdmin.password
        request
            .post('/api/v1/admin')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Admin account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                expect(data).toHaveProperty('role', 'ADMIN')
                done()
            })
    })
})

describe('Create initial user', () => {
    it('should create new user', async done => {
        staticUser.password_confirmation = staticUser.password
        request
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                expect(res.status).toBe(201)
                let { status, message, data } = res.body
                expect(status).toBe(true)
                expect(message).toBe("Your account has been created")
                expect(data).toHaveProperty('name')
                expect(data).toHaveProperty('username')
                expect(data).toHaveProperty('email')
                expect(data).toHaveProperty('token')
                done()
            })
    })
})

describe('POST /api/v1/articles', () => {
    it('should create a new article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .attach('image', `${__dirname}/../TestImage.jpg`)
                    .field('title', staticArticle.title)
                    .field('body', staticArticle.body)
                    .field('tags', staticArticle.tags)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })

    it('should create a new article with same tags', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                staticArticle.title = 'Article no 2'
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .attach('image', `${__dirname}/../TestImage.jpg`)
                    .field('title', staticArticle.title)
                    .field('body', staticArticle.body)
                    .field('tags', staticArticle.tags)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })

    it('should not create a new article without image due to missing information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                let articleSample = articleFixtures.create()
                delete articleSample.title
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .attach('image', `${__dirname}/../TestImage.jpg`)
                    .field('body', articleSample.body)
                    .field('tags', articleSample.tags)
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe('Failed to create article')
                        expect(data).toBe(null)
                        expect(errors).toHaveProperty('name', 'ValidationError')
                        done()
                    })
            })
    })

    it('should create a new article without image', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .field('title', staticArticle2.title)
                    .field('body', staticArticle2.body)
                    .field('tags', staticArticle2.tags)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })

    it('should create a new article without image and same tags', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                staticArticle2.title = 'Article Without Image 2'
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .field('title', staticArticle2.title)
                    .field('body', staticArticle2.body)
                    .field('tags', staticArticle2.tags)
                    .then(res => {
                        expect(res.status).toBe(201)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Article has been successfully created')
                        expect(data).toHaveProperty('title')
                        expect(data).toHaveProperty('body')
                        expect(data).toHaveProperty('author', id)
                        done()
                    })
            })
    })

    it('should not create a new article without image due to missing information', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                let articleSample = articleFixtures.create()
                delete articleSample.title
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .field('body', articleSample.body)
                    .field('tags', articleSample.tags)
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe('Failed to create article')
                        expect(data).toBe(null)
                        expect(errors).toHaveProperty('name', 'ValidationError')
                        done()
                    })
            })
    })

    it('should not create a new article because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .post('/api/v1/articles')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'Bearer ' + token)
                    .send(JSON.stringify(staticArticle))
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe('Failed to create article')
                        expect(data).toBe(null)
                        expect(errors).toBe('You are not allowed to create article')
                        done()
                    })
            })
    })
})

describe('GET /api/v1/articles', () => {
    it('should show all articles', async done => {
        request
            .get('/api/v1/articles')
            .query({ pagination: false })
            .then(res => {
                let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                let page = Math.ceil(Math.random() * (lastPage))
                request
                    .get('/api/v1/articles')
                    .query({ page: page })
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Successfully show all articles')
                        expect(data).toHaveProperty('docs')
                        expect(data).toHaveProperty('totalDocs')
                        done()
                    })
            })
    })

    it('should show page 1 of all articles', async done => {
        request
            .get('/api/v1/articles')
            .query({ pagination: false })
            .then(res => {
                let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                let page = lastPage + 1
                request
                    .get('/api/v1/articles')
                    .query({ page: page })
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe('Successfully show all articles')
                        expect(data).toHaveProperty('docs')
                        expect(data).toHaveProperty('totalDocs')
                        done()
                    })
            })
    })
})

describe('GET /api/v1/articles/mine', () => {
    it('should show page 1 of my articles', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = lastPage + 1
                        request
                            .get('/api/v1/articles/mine')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ page: page })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe('Successfully show my articles')
                                expect(data).toHaveProperty('docs')
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })
})

describe('GET /api/v1/articles/view', () => {
    it('should show one specific article', async done => {
        request
            .get('/api/v1/articles')
            .query({ pagination: false })
            .then(res => {
                let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                let article = res.body.data.docs[i]
                let articleId = article._id
                request
                    .get('/api/v1/articles/view')
                    .query({articleId: articleId})
                    .then(res => {
                        expect(res.status).toBe(200)
                        let { status, message, data } = res.body
                        expect(status).toBe(true)
                        expect(message).toBe("Successfully view one article")
                        expect(data).toHaveProperty('title', article.title)
                        expect(data).toHaveProperty('body', article.body)
                        done()
                    })
            })
    })

    it('should not show one specific article due to invalid articleId', async done => {
        request
            .get('/api/v1/articles')
            .query({ pagination: false })
            .then(res => {
                let articleId = 'articleId'
                request
                    .get('/api/v1/articles/view')
                    .query({articleId: articleId})
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe("Failed to view one article")
                        expect(data).toBe(null)
                        expect(errors).toHaveProperty('name', 'CastError')
                        done()
                    })
            })
    })
})

describe('PUT /api/v1/articles', () => {
    it('should update an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        let update = articleFixtures.create()
                        delete update.title
                        request
                            .put('/api/v1/articles')
                            .set("Content-Type", "mulipart/form-data")
                            .set('Authorization', 'Bearer ' + token)
                            .attach('image', `${__dirname}/../TestImage.jpg`)
                            .field('body', update.body)
                            .field('tags', update.tags)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Article has been successfully updated")
                                expect(data).toHaveProperty('body', update.body)
                                expect(data).toHaveProperty('updatedBy', id)
                                done()
                            })
                    })
            })
    })

    it('should not update an article without picture due to invalid articleId', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let articleId = 'articleId'
                        let update = articleFixtures.create()
                        request
                            .put('/api/v1/articles')
                            .set("Content-Type", "mulipart/form-data")
                            .set('Authorization', 'Bearer ' + token)
                            .attach('image', `${__dirname}/../TestImage.jpg`)
                            .field('body', update.body)
                            .field('tags', update.tags)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to update article")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should update article without picture', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        let update = articleFixtures.create()
                        delete update.title
                        request
                            .put('/api/v1/articles')
                            .set("Content-Type", "mulipart/form-data")
                            .set('Authorization', 'Bearer ' + token)
                            .field('body', update.body)
                            .field('tags', update.tags)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Article has been successfully updated")
                                expect(data).toHaveProperty('body', update.body)
                                expect(data).toHaveProperty('updatedBy', id)
                                done()
                            })
                    })
            })
    })

    it('should not update an article without picture due to invalid articleId', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let articleId = 'articleId'
                        let update = articleFixtures.create()
                        request
                            .put('/api/v1/articles')
                            .set("Content-Type", "mulipart/form-data")
                            .set('Authorization', 'Bearer ' + token)
                            .field('body', update.body)
                            .field('tags', update.tags)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to update article")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should not update an article because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        let update = articleFixtures.create()
                        request
                            .post('/api/v1/auth')
                            .set('Content-Type', 'application/json')
                            .send(JSON.stringify(staticUser))
                            .then(res => {
                                token = res.body.data.token
                                request
                                    .put('/api/v1/articles')
                                    .set('Content-Type', 'application/json')
                                    .set('Authorization', 'Bearer ' + token)
                                    .query({ articleId: articleId })
                                    .send(JSON.stringify(update))
                                    .then(res => {
                                        expect(res.status).toBe(422)
                                        let { status, message, data, errors } = res.body
                                        expect(status).toBe(false)
                                        expect(message).toBe('Failed to update article')
                                        expect(data).toBe(null)
                                        expect(errors).toBe("You're not allowed to update article")
                                        done()
                                    })
                            })
                    })
            })
    })
})

describe('GET /api/v1/articles/tag', () => {
    it('should filter articles by chosen tag', async done => {
        request
            .get('/api/v1/tags')
            .then(res => {
                let i = Math.floor(Math.random() * (res.body.data.length - 1))
                let tag = res.body.data[i]
                let tagId = tag._id
                request
                    .get('/api/v1/articles/tag')
                    .query({ tagId: tagId, pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = Math.ceil(Math.random() * (lastPage))
                        request
                            .get('/api/v1/articles/tag')
                            .query({ tagId: tagId, page: page })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully filtered by tag")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    })
            })
    })

    it('should show page 1 of filtered articles by chosen tag', async done => {
        request
            .get('/api/v1/tags')
            .then(res => {
                let i = Math.floor(Math.random() * (res.body.data.length - 1))
                let tag = res.body.data[i]
                let tagId = tag._id
                request
                    .get('/api/v1/articles/tag')
                    .query({ tagId: tagId, pagination: false })
                    .then(res => {
                        let lastPage = Math.ceil(res.body.data.totalDocs / 10)
                        let page = lastPage + 1
                        request
                            .get('/api/v1/articles/tag')
                            .query({ tagId: tagId, page: page })
                            .then(res => {
                                expect(res.status).toBe(200)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully filtered by tag")
                                expect(data).toHaveProperty('totalDocs')
                                done()
                            })
                    }
                    )
            })
    })

    it('should not filter articles by chosen tag', async done => {
        request
            .get('/api/v1/tags')
            .then(res => {
                let tagId = 'tagId'
                request
                    .get('/api/v1/articles/tag')
                    .query({ tagId: tagId })
                    .then(res => {
                        expect(res.status).toBe(422)
                        let { status, message, data, errors } = res.body
                        expect(status).toBe(false)
                        expect(message).toBe("Failed to filter by tag")
                        expect(data).toBe(null)
                        expect(errors).toHaveProperty('name', 'CastError')
                        done()
                    }
                    )
            })
    })
})

describe('POST /api/v1/articles/like', () => {
    it('should successfully like an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles')
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let article = res.body.data.docs[i]
                        let articleId = article._id
                        request
                            .post('/api/v1/articles/like')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Successfully liked article")
                                expect(data).toHaveProperty('_id', articleId)
                                expect(data).toHaveProperty('title', article.title)
                                expect(data).toHaveProperty('likes')
                                done()
                            })
                    })
            })
    })

    it('should not successfully like an article because user previously liked the article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                let id = res.body.data._id
                let articleSample = articleFixtures.create()
                request
                    .post('/api/v1/articles')
                    .set("Content-Type", "mulipart/form-data")
                    .set('Authorization', 'Bearer ' + token)
                    .field('title', articleSample.title)
                    .field('body', articleSample.body)
                    .field('tags', articleSample.tags)
                    .then(res => {
                        let articleId = res.body.data._id
                        request
                            .post('/api/v1/articles/like')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ articleId: articleId })
                            .then(res => {
                                request
                                    .post('/api/v1/articles/like')
                                    .set('Authorization', 'Bearer ' + token)
                                    .query({ articleId: articleId })
                                    .then(res => {
                                        expect(res.status).toBe(422)
                                        let { status, message, data, errors } = res.body
                                        expect(status).toBe(false)
                                        expect(message).toBe("Failed to like article")
                                        expect(data).toBe(null)
                                        expect(errors).toBe("You've already liked this article")
                                        done()
                                    })
                            })
                    })
            })
    })

    it('should not successfully like an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticUser))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles')
                    .query({ pagination: false })
                    .then(res => {
                        let articleId = 'articleId'
                        request
                            .post('/api/v1/articles/like')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to like article")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })
})

describe('DELETE /api/v1/articles', () => {
    it('should not delete an article because user is not admin', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        request
                            .post('/api/v1/auth')
                            .set('Content-Type', 'application/json')
                            .send(JSON.stringify(staticUser))
                            .then(res => {
                                token = res.body.data.token
                                request
                                    .delete('/api/v1/articles')
                                    .set('Authorization', 'Bearer ' + token)
                                    .query({ articleId: articleId })
                                    .then(res => {
                                        expect(res.status).toBe(422)
                                        let { status, message, data, errors } = res.body
                                        expect(status).toBe(false)
                                        expect(message).toBe("Failed to delete article")
                                        expect(data).toBe(null)
                                        expect(errors).toBe("You're not allowed to delete article")
                                        done()
                                    })
                            })
                    })
            })
    })

    it('should not delete an article due to invalid articleId', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let articleId = 'articleId'
                        request
                            .delete('/api/v1/articles')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(422)
                                let { status, message, data, errors } = res.body
                                expect(status).toBe(false)
                                expect(message).toBe("Failed to delete article")
                                expect(data).toBe(null)
                                expect(errors).toHaveProperty('name', 'CastError')
                                done()
                            })
                    })
            })
    })

    it('should delete an article', async done => {
        request
            .post('/api/v1/auth')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(staticAdmin))
            .then(res => {
                let token = res.body.data.token
                request
                    .get('/api/v1/articles/mine')
                    .set('Authorization', 'Bearer ' + token)
                    .query({ pagination: false })
                    .then(res => {
                        let i = Math.floor(Math.random() * (res.body.data.docs.length - 1))
                        let articleId = res.body.data.docs[i]._id
                        request
                            .delete('/api/v1/articles')
                            .set('Authorization', 'Bearer ' + token)
                            .query({ articleId: articleId })
                            .then(res => {
                                expect(res.status).toBe(201)
                                let { status, message, data } = res.body
                                expect(status).toBe(true)
                                expect(message).toBe("Article has been successfully deleted")
                                expect(data).toHaveProperty('_id', articleId)
                                done()
                            })
                    })
            })
    })
})
