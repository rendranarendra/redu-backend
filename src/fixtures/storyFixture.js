const faker = require('faker')

function create() {
    return {
        title: faker.lorem.word(),
        body: faker.lorem.paragraph(),
        category: "5e7cd14b1de15809ad84034a",
        location: faker.random.word(),
        long: 40.71559738029149,
        lat: -73.9600613197085
    }
}

module.exports = {
    create
}