const faker = require('faker')

function create() {
    return {
        name: faker.name.firstName(),
    }
}

module.exports = {
    create
}