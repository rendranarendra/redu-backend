const faker = require('faker')

function create(origin) {
    return {
        message: faker.lorem.sentence(),
        origin: origin,
}}

module.exports = {
    create
}