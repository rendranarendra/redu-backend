const faker = require('faker')

function create() {
    return {
        name: faker.name.findName(),
        username: faker.name.firstName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        password_confirmation: faker.internet.password(),
        bio: faker.random.words(),
    }
}

module.exports = {
    create
}