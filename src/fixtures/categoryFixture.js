const faker = require('faker')

function create() {
    return {
        name: faker.lorem.words(),
    }
}

module.exports = {
    create
}