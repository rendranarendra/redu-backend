const faker = require('faker')

function create() {
    return {
        title: faker.lorem.words(),
        body: faker.lorem.sentence(),
        tags: `${faker.random.word()}, ${faker.random.word()}, ${faker.random.word()}`
    }
}

module.exports = {
    create
}