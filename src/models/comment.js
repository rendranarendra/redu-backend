const Article = require('./article.js')
const Story = require('./story.js')

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2');

const commentSchema = new Schema({
    message: {
        type: String,
        required: true
    },
    previousMessages: [{
        type: String,
    }],
    originId: {
        type: Schema.Types.ObjectId,
        refPath: 'originModel'
    },
    origin: {
        type: String,
        required: true,
        enum: ['Article', 'Story']
    },
    commenter: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
},
    {
        timestamps: true,
        versionKey: false
    })

commentSchema.plugin(mongoosePaginate)

class Comment extends mongoose.model('Comment', commentSchema) {
    static newComment(commenter, originId, bodyParams) {
        return new Promise((resolve, reject) => {
            let params = {
                message: bodyParams.message,
                origin: bodyParams.origin,
                originId: originId,
                commenter: commenter,
            }

            let pushData

            this.create(params)
                .then(data => {

                    pushData = data._id

                    if (params.origin == 'Article') {
                        Article.findById(data.originId)
                            .then(article => {
                                article.comments.push(pushData)
                                article.save()
                            })
                    }

                    else if (params.origin == 'Story') {
                        Story.findById(data.originId)
                            .then(story => {
                                story.comments.push(pushData)
                                story.save()
                            })
                    }
                    resolve({
                        _id: data._id,
                        message: data.message,
                        originId: data.originId,
                        createdAt: data.createdAt,
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static showMyComments(commenter, origin, page, pagination) {
        return new Promise((resolve, reject) => {
            let options = {
                page: page,
                limit: 10,
                pagination: JSON.parse(pagination),
                sort: '-updatedAt',
            }

            switch (origin) {
                case 'Article':
                    this.find({ commenter: commenter, origin: 'Article' })
                        .then(data => {
                            let lastPage = Math.floor(data.length / 10) + 1
                            if (options.page > lastPage || options.page < 0) options.page = 1

                            this.paginate({ commenter: commenter, origin: 'Article' }, options)
                                .then(data => {
                                    resolve(data)
                                })
                        })
                    break

                case 'Story':
                    this.find({ commenter: commenter, origin: 'Story' })
                        .then(data => {
                            let lastPage = Math.floor(data.length / 10) + 1
                            if (options.page > lastPage || options.page < 0) options.page = 1

                            this.paginate({ commenter: commenter, origin: 'Story' }, options)
                                .then(data => {
                                    resolve(data)
                                })
                        })
                    break

                default:
                    this.find({ commenter: commenter })
                        .then(data => {
                            let lastPage = Math.floor(data.length / 10) + 1
                            if (options.page > lastPage || options.page < 0) options.page = 1

                            this.paginate({ commenter: commenter }, options)
                                .then(data => {
                                    resolve(data)
                                })
                        })
            }

        })
    }

    static editComment(commenter, commentId, bodyParams) {
        return new Promise((resolve, reject) => {
            let previousMessage

            this.findById(commentId)
                .then(comment => {
                    previousMessage = comment.message

                    let params = {
                        message: bodyParams.message,
                        origin: bodyParams.origin,
                    }

                    this.findOneAndUpdate({ _id: commentId, commenter: commenter }, params, { new: true })
                        .then(data => {
                            data.previousMessages.push(previousMessage)
                            data.save()

                            resolve({
                                _id: data._id,
                                previousMessage: data.previousMessages[data.previousMessages.length - 1],
                                message: data.message,
                                updatedAt: data.updatedAt
                            })
                        })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static deleteComment(commenter, commentId) {
        return new Promise((resolve, reject) => {
            this.findOneAndDelete({ _id: commentId, commenter: commenter })
                .then(data => {
                    resolve(data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}

module.exports = Comment
