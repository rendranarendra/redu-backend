const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2');

const tagSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    previousName: {
        type: String,
    },
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},
    {
        timestamps: true,
        versionKey: false,
    })

tagSchema.plugin(mongoosePaginate)

class Tag extends mongoose.model('Tag', tagSchema) {
    static createTag(role, bodyParams) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You're not allowed to add tag")

            let params = {
                name: bodyParams.name
            }

            this.create(params)
                .then(data => {
                    resolve({
                        _id: data._id,
                        name: data.name,
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static updateTag(role, tagId, bodyParams) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You're not allowed to update tag")

            this.findById(tagId)
                .then(tag => {
                    let update = {
                        name: bodyParams.name,
                    }
                    update.previousName = tag.name

                    this.findByIdAndUpdate(tagId, update, { new: true })
                        .then(data => {
                            resolve({
                                _id: data._id,
                                previousName: data.previousName,
                                name: data.name,
                                updatedAt: data.updatedAt
                            })
                        })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static deleteTag(role, tagId) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You're not allowed to delete tag")

            this.findByIdAndDelete(tagId)
                .then(data => {
                    resolve({
                        _id: data._id,
                        name: data.name,
                        status: "Deleted",
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}

module.exports = Tag
