const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    name: {
      type: String
    }
  },
  {
    versionKey: false
  }
);

class Category extends mongoose.model("Category", categorySchema) {
  static new(role, bodyParams) {
    return new Promise((resolve, reject) => {
      if (role != "ADMIN")
        return reject("You're not allowed to create category");
      let params = {
        name: bodyParams.name
      };
      this.create(params)
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static all() {
    return new Promise((resolve, reject) => {
      this.find({})
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static update(role, categoryId, bodyParams) {
    return new Promise((resolve, reject) => {
      if (role != "ADMIN")
        return reject("You're not allowed to update category");
      let params = {
        name: bodyParams.name
      };

      this.findOneAndUpdate({ _id: categoryId }, params, { new: true })
        .then(data => {
          if (!data) return reject("Category not found!")
          resolve({
            _id: data._id,
            name: data.name
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static delete(role, categoryId) {
    return new Promise((resolve, reject) => {
      if (role != "ADMIN") reject("You're not allowed to delete category");
      console.log("DEBUG", categoryId);
      this.findByIdAndDelete({ _id: categoryId })
        .then(data => {
          resolve({
            _id: data._id,
            name: data.name
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

module.exports = Category;
