const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const Location = require("./location.js");

const Schema = mongoose.Schema;
const Imagekit = require("imagekit");
const imageKit = new Imagekit({
  publicKey: process.env.PUBLIC_KEY,
  privateKey: process.env.PRIVATE_KEY,
  urlEndpoint: process.env.URL_ENDPOINT
});

const storySchema = new Schema(
  {
    author: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    title: {
      type: "string",
      required: true
    },
    body: {
      type: "string",
      required: true
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: "Category"
    },
    location: {
      type: Schema.Types.ObjectId,
      ref: "Location"
    },
    image: {
      type: "string",
      default:
        "https://upload.wikimedia.org/wikipedia/commons/8/8e/Arjuno-Welirang_from_Mount_Penanggungan_by_Harris_Frilingga_K.jpg"
    },
    likes: [
      {
        type: Schema.Types.ObjectId,
        ref: "User"
      }
    ],
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: "Comment"
      }
    ]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

storySchema.plugin(mongoosePaginate);
storySchema.plugin(require("mongoose-autopopulate"));

/* Create story with or without image */
class Stories extends mongoose.model("Stories", storySchema) {
  static add(data) {
    return new Promise((resolve, reject) => {
      let { location: locationName, image, long, lat } = data;
      Location.findOne({
        long,
        lat
      })
        .then(async location => {
          if (!location)
            location = await Location.create({
              long,
              lat,
              name: locationName
            });

          if (image) {
            image = await imageKit.upload({
              file: image.buffer.toString("base64"),
              fileName: `IMG-${Date.now()}`
            });

            data.image = image.url;
          }

          data.location = location;
          return super.create(data);
        })
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

  /* List user story by id */
  static findStory(author, page, pagination, title, body, category, location) {
    return new Promise((resolve, reject) => {
      let options = {
        page: page,
        limit: 9,
        pagination: JSON.parse(pagination),
        sort: "-createdAt",
        select: [
          "title",
          "image",
          "category",
          "location",
          "author",
          "image",
          "createdAt"
        ],
        populate: [
          { path: "category", select: ["name"] },
          { path: "category", select: ["name"] },
          { path: "location", select: ["name", "long", "lat"] }
        ]
      };

      let params = {
        author: author,
        title: title,
        body: body,
        category: category,
        location: location
      };

      /// Deleting the unused field
      for (let prop in params) if (!params[prop]) delete params[prop];

      this.find(params).then(data => {
        let lastPage = Math.ceil(data.length / 9);
        if (lastPage == 0) lastPage = 1;
        if (options.page > lastPage || options.page < 0) options.page = 1;

        this.paginate({ author: author }, options)
          .then(data => {
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    });
  }

  /* List all users story without login */
  static findAll(page, pagination) {
    return new Promise((resolve, reject) => {
      let options = {
        page: page,
        limit: 9,
        pagination: JSON.parse(pagination),
        sort: "-createdAt",
        select: [
          "title",
          "image",
          "body",
          "category",
          "comments",
          "location",
          "author",
          "createdAt"
        ],
        populate: [
          { path: "category", select: ["name"] },
          { path: "author", select: ["name", "image"] },
          { path: "likes", select: ["name"] },
          { path: "location", select: ["name", "long", "lat"] }
        ]
      };

      this.find({}).then(data => {
        let lastPage = Math.ceil(data.length / 9);
        if (lastPage == 0) lastPage = 1;
        if (options.page > lastPage || options.page < 0) options.page = 1;

        this.paginate({}, options)
          .then(data => {
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    });
  }

  /* Sort User's story */
  static mySort(author, order, sort, page) {
    return new Promise((resolve, reject) => {
      let options = {};
      switch (order) {
        case "ascending":
          options = {
            page: page,
            limit: 9,
            sort: `${sort}`
          };
          if (
            ["title", "createdAt", "body", "category", "location"].indexOf(
              options.sort
            ) < 0
          ) {
            return reject("Invalid sorting parameter!");
          }

          this.find({ author: author }).then(data => {
            let lastPage = Math.ceil(data.length / 9);
            if (lastPage === 0) lastPage = 1;
            if (options.page > lastPage || options.page < 0) options.page = 1;

            this.paginate({ author: author }, options).then(data => {
              resolve(data);
            });
          });

          break;

        case "descending":
          options = {
            page: page,
            limit: 9,
            sort: `-${sort}`
          };

          if (
            ["-title", "-createdAt", "-body", "-category", "-location"].indexOf(
              options.sort
            ) < 0
          ) {
            return reject("Invalid sorting parameter!");
          }

          this.find({ author: author }).then(data => {
            let lastPage = Math.ceil(data.length / 9);
            if (lastPage === 0) lastPage = 1;
            if (options.page > lastPage || options.page < 0) options.page = 1;

            this.paginate({ author: author }, options).then(data => {
              resolve(data);
            });
          });

          break;

        default:
          return reject("Invalid sorting order");
      }
    });
  }

  /* Delete an exsisting user's story */
  static destroy(author, Storyid) {
    return new Promise((resolve, reject) => {
      this.findById(Storyid)
        .then(data => {
          if (data.author != author)
            return reject("You are not authorized to do this!");
          this.findByIdAndDelete(Storyid).then(data => {
            resolve(`Story ${data.title} successfully deleted!`);
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /* Update an exsisting user's story */
  static edit(author, storyId, formParams, formFile) {
    return new Promise((resolve, reject) => {
      let params = {
        title: formParams.title,
        body: formParams.body,
        category: formParams.category
      };

      for (let prop in params) if (!params[prop]) delete params[prop];

      if (!formFile) {
        this.findOneAndUpdate({ author: author, _id: storyId }, params, {
          new: true
        })
          .then(data => {
            if(!data)
            return reject ("Invalid story or author")
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      } else {
        imageKit
          .upload({
            file: formFile.buffer.toString("base64"),
            fileName: `IMG-${Date.now()}`
          })
          .then(data => {
            this.findOneAndUpdate(
              { author: author, _id: storyId },
              { $set: { ...params, image: data.url } },
              { new: true }
            ).then(res => {
              if(!res)
              return reject ("Invalid story or author")
              resolve(res);
            });
          })
          .catch(err => {
            reject(err);
          });
      }
    });
  }

  static likeStory(liker, storyId) {
    return new Promise((resolve, reject) => {
      this.findById(storyId)
        .then(data => {
          if (data.likes.indexOf(liker) >= 0)
            return reject("You've already liked this story");

          data.likes.push(liker);
          data.save();
          resolve({
            _id: data._id,
            title: data.title,
            likes: data.likes
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static filterCategory(category, page) {
    return new Promise((resolve, reject) => {
      let options = {
        page: page,
        limit: 9,
        pagination: true,
        populate: [
          { path: "category", select: ["name"] },
          { path: "author", select: ["name", "image"] },
          {
            path: "comments",
            select: ["commenter", "message", "createdAt", "updatedAt"]
          },
          { path: "commenter", select: ["name", "image"] },
          { path: "location", select: ["name", "long", "lat"] }
        ],
        sort: "updatedAt",
        collation: { locale: "en" }
      };

      this.find({ category: category })
        .then(data => {
          if (data.length == 0)
            return reject(`There is no story with category ${category}`);
          let lastPage = Math.floor(data.length / 9) + 1;
          if (options.page > lastPage || options.page < 0) options.page = 1;

          this.paginate({ category: category }, options).then(data => {
            resolve(data);
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static findByID(storyId) {
    return new Promise((resolve, reject) => {
      this.find({ _id: storyId })
        .populate([
          { path: "author", select: ["name", "image"] },
          { path: "category", select: ["name"] },
          { path: "likes", select: ["name"] },
          {
            path: "comments",
            populate: { path: "commenter", select: ["name", "image"] },
            select: ["commenter", "message", "createdAt", "updatedAt"]
          },

          { path: "location", select: ["name", "long", "lat"] }
        ])
        .select("-password")
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

module.exports = Stories;
