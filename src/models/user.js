const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const Imagekit = require("imagekit");
const imageKit = new Imagekit({
    publicKey: process.env.PUBLIC_KEY,
    privateKey: process.env.PRIVATE_KEY,
    urlEndpoint: process.env.URL_ENDPOINT
})

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        requiredl: true,
        unique: true,
    },
    image: {
        type: String,
        default: 'https://ik.imagekit.io/rendranarendra/IMG-1584035165791_ixW9Mdk38',
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    bio: {
        type: String,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        enum: ["ADMIN", "USER"],
        default: "USER",
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    stories: [{
        type: Schema.Types.ObjectId,
        ref: 'Stories',
    }],
    articles: [{
        type: Schema.Types.ObjectId,
        ref: 'Articles',
    }],
}, {
    timestamps: true,
    versionKey: false,
})

class User extends mongoose.model('User', userSchema) {
    static register(bodyParams) {
        return new Promise((resolve, reject) => {
            if (bodyParams.password != bodyParams.password_confirmation) return reject("Password confirmation does not match!")

            let encrypted_password = bcrypt.hashSync(bodyParams.password, 10)

            let params = {
                name: bodyParams.name,
                username: bodyParams.username,
                email: bodyParams.email,
                password: encrypted_password
            }

            this.create(params)
                .then(data => {

                    let token = jwt.sign({ _id: data._id, role: data.role }, process.env.JWT_SIGNATURE_KEY)

                    resolve({
                        _id: data._id,
                        name: data.name,
                        username: data.username,
                        email: data.email,
                        token: token
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static login(bodyParams) {
        return new Promise((resolve, reject) => {
            let params = {
                username: bodyParams.username,
                password: bodyParams.password
            }

            this.findOne({ username: params.username })
                .then(data => {
                    if (!data) return reject("User not found!")

                    if (bcrypt.compareSync(params.password, data.password)) {
                        let token = jwt.sign({ _id: data._id, role: data.role }, process.env.JWT_SIGNATURE_KEY)

                        resolve({
                            _id: data._id,
                            username: data.username,
                            name: data.name,
                            token: token
                        })
                    }

                    else {
                        return reject("Incorrect Password!")
                    }
                })
        })
    }

    static me(user) {
        return new Promise((resolve) => {
            this.findById(user)
                .select(['name', 'username', 'email', 'image', 'bio'])
                .then(data => {
                    resolve(data)
                })
        })
    }

    static edit(user, bodyParams, imageParams) {
        return new Promise((resolve, reject) => {
            let params = {
                name: bodyParams.name,
                bio: bodyParams.bio,
            }
            for (let prop in params) if (!params[prop]) delete params[prop]

            if (!imageParams) {
                this.findByIdAndUpdate(user, params, { new: true })
                    .then(data => {
                        resolve({
                            name: data.name,
                            username: data.username,
                            email: data.email,
                            bio: data.bio,
                        })
                    })
            }
            else {
                imageKit
                    .upload({
                        file: imageParams.buffer.toString("base64"),
                        fileName: `IMG-${Date.now()}`
                    })
                    .then(image => {
                        params.image = image.url
                        this.findByIdAndUpdate(user, params, { new: true })
                            .then(data => {
                                resolve({
                                    name: data.name,
                                    username: data.username,
                                    email: data.email,
                                })
                            })
                    })
            }
        })
    }

    static delete(user) {
        return new Promise((resolve, reject) => {
            this.findById(user)
                .then(data => {
                    data.isActive = false
                    data.save()
                    resolve({
                        isActive: data.isActive
                    })
                })
        })
    }

    static registerAdmin(bodyParams) {
        return new Promise((resolve, reject) => {
            if (bodyParams.password != bodyParams.password_confirmation) reject("Password confirmation does not match!")

            let encrypted_password = bcrypt.hashSync(bodyParams.password, 10)

            let params = {
                name: bodyParams.name,
                username: bodyParams.username,
                email: bodyParams.email,
                password: encrypted_password,
                role: "ADMIN"
            }

            this.create(params)
                .then(data => {

                    let token = jwt.sign({ _id: data._id, role: data.role }, process.env.JWT_SIGNATURE_KEY)

                    resolve({
                        _id: data._id,
                        name: data.name,
                        username: data.username,
                        email: data.email,
                        role: data.role,
                        token: token

                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}

module.exports = User
