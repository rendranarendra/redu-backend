const Tag = require('./tag.js')
const Imagekit = require("imagekit");
const imageKit = new Imagekit({
    publicKey: process.env.PUBLIC_KEY,
    privateKey: process.env.PRIVATE_KEY,
    urlEndpoint: process.env.URL_ENDPOINT
});

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2');

const articleSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true,
    },
    body: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        default: "https://upload.wikimedia.org/wikipedia/commons/8/8e/Arjuno-Welirang_from_Mount_Penanggungan_by_Harris_Frilingga_K.jpg",
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    likes: [{
        type: Schema.Types.ObjectId,
        ref: "User",
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: "Comment",
    }],
    tags: [{
        type: Schema.Types.ObjectId,
        ref: "Tag",
    }]
},
    {
        timestamps: true,
        versionKey: false,
    })

articleSchema.plugin(mongoosePaginate)
articleSchema.plugin(require('mongoose-autopopulate'))

class Article extends mongoose.model('Article', articleSchema) {
    static newArticle(author, role, bodyParams, imageParams) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You are not allowed to create article")

            let tags = []
            tags = bodyParams.tags.split(", ")

            let params = {
                title: bodyParams.title,
                body: bodyParams.body,
                author: author,
                updatedBy: author,
            }

            if (!imageParams) {
                this.create(params)
                    .then(data => {
                        tags.forEach(element => {
                            Tag.findOne({ name: element })
                                .then(result => {
                                    if (!result) {
                                        Tag.create({ name: element })
                                            .then(async (newTag) => {
                                                this.findByIdAndUpdate(data._id, { $push: { tags: newTag._id } }, { new: true })
                                                    .then(() => { })
                                            })
                                    }

                                    else if (result) {
                                        this.findByIdAndUpdate(data._id, { $push: { tags: result._id } }, { new: true })
                                            .then(() => { })
                                    }
                                })
                        })

                        resolve({
                            _id: data._id,
                            title: data.title,
                            body: data.body,
                            author: data.author,
                            tags: tags,
                            createdAt: data.createdAt,
                        })
                    })
                    .catch(err => {
                        reject(err)
                    })
            }

            else {
                imageKit
                    .upload({
                        file: imageParams.buffer.toString("base64"),
                        fileName: `IMG-${Date.now()}`
                    })
                    .then(image => {
                        params.image = image.url
                        this.create(params)
                            .then(data => {
                                tags.forEach(element => {
                                    Tag.findOne({ name: element })
                                        .then(result => {
                                            if (!result) {
                                                Tag.create({ name: element })
                                                    .then(async (newTag) => {
                                                        this.findByIdAndUpdate(data._id, { $push: { tags: newTag._id } }, { new: true })
                                                            .then(() => { })
                                                    })
                                            }

                                            else if (result) {
                                                this.findByIdAndUpdate(data._id, { $push: { tags: result._id } }, { new: true })
                                                    .then(() => { })
                                            }
                                        })
                                })

                                resolve({
                                    _id: data._id,
                                    title: data.title,
                                    body: data.body,
                                    author: data.author,
                                    tags: tags,
                                    createdAt: data.createdAt,
                                })
                            })
                            .catch(err => {
                                reject(err)
                            })
                    })
            }
        })
    }

    static allArticle(page, pagination) {
        return new Promise((resolve, reject) => {
            let options = {
                page: page,
                limit: 9,
                pagination: JSON.parse(pagination),
                sort: '-updatedAt',
                populate: [
                    { path: 'likes', select: ['name'] },
                    { path: 'tags', select: ['name'] },
                    { path: 'comments', select: ['commenter', 'message', 'createdAt', 'updatedAt'], populate: { path: 'commenter', select: ['name', 'image'] } },
                ]
            }

            this.find({})
                .then(data => {
                    let lastPage = Math.floor(data.length / 9) + 1
                    if (options.page > lastPage || options.page < 0) options.page = 1

                    this.paginate({}, options)
                        .then(data => {
                            resolve(data)
                        })
                })
        })
    }

    static myArticle(author, page, pagination) {
        return new Promise((resolve) => {
            let options = {
                page: page,
                limit: 9,
                pagination: JSON.parse(pagination),
                sort: '-updatedAt',
            }

            this.find({ author: author })
                .then(data => {
                    let lastPage = Math.floor(data.length / 9) + 1
                    if (options.page > lastPage || options.page < 0) options.page = 1

                    this.paginate({ author: author }, options)
                        .then(data => {
                            resolve(data)
                        })
                })
        })
    }

    static filterTag(tagId, page, pagination) {
        return new Promise((resolve, reject) => {
            let options = {
                page: page,
                limit: 9,
                pagination: JSON.parse(pagination),
                sort: 'updatedAt',
            }

            this.find({ tags: { "$in": [tagId] } })
                .then(data => {
                    let lastPage = Math.floor(data.length / 9) + 1
                    if (options.page > lastPage || options.page < 0) options.page = 1

                    this.paginate({ tags: { "$in": [tagId] } }, options)
                        .then(data => {
                            resolve(data)
                        })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static update(editor, role, articleId, bodyParams, imageParams) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You're not allowed to update article")
            let params = {
                title: bodyParams.title,
                body: bodyParams.body,
                updatedBy: editor
            }
            for (let prop in params) if (!params[prop]) delete params[prop]

            if (!imageParams) {
                this.findOneAndUpdate({ _id: articleId }, params, { new: true })
                    .then(data => {
                        resolve({
                            _id: data._id,
                            title: data.title,
                            body: data.body,
                            author: data.author,
                            updatedBy: editor,
                            updatedAt: data.updatedAt,
                        })
                    })
                    .catch(err => {
                        reject(err)
                    })
            }

            else {
                imageKit
                    .upload({
                        file: imageParams.buffer.toString("base64"),
                        fileName: `IMG-${Date.now()}`
                    })
                    .then(image => {
                        params.image = image.url
                        this.findOneAndUpdate({ _id: articleId }, params, { new: true })
                            .then(data => {
                                resolve({
                                    _id: data._id,
                                    title: data.title,
                                    body: data.body,
                                    author: data.author,
                                    updatedBy: editor,
                                    updatedAt: data.updatedAt,
                                })
                            })
                            .catch(err => {
                                reject(err)
                            })
                    })
            }

        })
    }

    static destroy(author, role, articleId) {
        return new Promise((resolve, reject) => {
            if (role != 'ADMIN') return reject("You're not allowed to delete article")
            this.findOneAndDelete({ _id: articleId, author: author })
                .then(data => {
                    resolve({
                        _id: data._id,
                        title: data.title,
                        body: data.body,
                        author: data.author,
                        status: "Deleted"
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static likeArticle(liker, articleId) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then(data => {

                    if (data.likes.indexOf(liker) >= 0) return reject("You've already liked this article")

                    data.likes.push(liker)
                    data.save()
                    resolve({
                        _id: data._id,
                        title: data.title,
                        likes: data.likes,
                    })
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}

module.exports = Article
