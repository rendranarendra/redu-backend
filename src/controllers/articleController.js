const Article = require('../models/article.js')
const {
    success,
    error
} = require('../helpers/response.js')

exports.create = async (req, res) => {
    try {
        let result = await Article.newArticle(req.user._id, req.user.role, req.body, req.file)
        let message = "Article has been successfully created"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to create article"
        error(res, message, err, 422)
    }
}

exports.view = async (req, res) => {
    let result = await Article.allArticle(req.query.page || 1, req.query.pagination || true)
    let message = "Successfully show all articles"
    success(res, message, result, 200)
}

exports.viewOne = async (req, res) => {
    await Article.findById(req.query.articleId)
        .populate({ path: 'likes', select: 'name' })
        .populate({ path: 'comments', populate: { path: 'commenter', select: ['name','image'] }, select: ['commenter', 'message', 'createdAt', 'updatedAt'] })
        .populate({ path: 'tags', select: 'name' })
        .then(data => {

            let result = {
                _id: data._id,
                title: data.title,
                body: data.body,
                image: data.image,
                likes: data.likes,
                comments: data.comments,
                tags: data.tags,
            }
            let message = "Successfully view one article"
            success(res, message, result, 200)

        })
        .catch(err => {
            let message = "Failed to view one article"
            error(res, message, err, 422)
        })
}

exports.mine = async (req, res) => {
    let result = await Article.myArticle(req.user._id, req.query.page || 1, req.query.pagination || true)
    let message = "Successfully show my articles"
    success(res, message, result, 200)
}

exports.tag = async (req, res) => {
    try {
        let result = await Article.filterTag(req.query.tagId, req.query.page || 1, req.query.pagination || true)
        let message = "Successfully filtered by tag"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to filter by tag"
        error(res, message, err, 422)
    }
}

exports.edit = async (req, res) => {
    try {
        let result = await Article.update(req.user._id, req.user.role, req.query.articleId, req.body, req.file)
        let message = "Article has been successfully updated"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to update article"
        error(res, message, err, 422)
    }
}

exports.purge = async (req, res) => {
    try {
        let result = await Article.destroy(req.user._id, req.user.role, req.query.articleId)
        let message = "Article has been successfully deleted"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to delete article"
        error(res, message, err, 422)
    }
}

exports.like = async (req, res) => {
    try {
        let result = await Article.likeArticle(req.user._id, req.query.articleId)
        let message = "Successfully liked article"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to like article"
        error(res, message, err, 422)
    }
}