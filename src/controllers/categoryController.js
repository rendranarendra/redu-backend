const category = require('../models/category.js')
const {success, error} = require('../helpers/response.js')

exports.createCategory = async (req, res) => {
    try {
        let result = await category.new(req.user.role, req.body)
        let message = "Successfully add new category"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to add new category"
        error(res, message, err, 422)
    }
}

exports.getAllCategory = async (req, res) => {
    try {
        let result = await category.all()
        let message = "Successfully get all categories"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to get categories"
        error(res, message, err, 422)
    }
}

exports.updateCategory = async (req, res) => {
    try {
        let result = await category.update(req.user.role, req.query.id, req.body)
        let message = "Successfully update a category"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to update category"
        error(res, message, err, 422)
    }
}

exports.deleteCategory = async (req, res) => {
    try {
        let result = await category.delete(req.user.role, req.query._id)
        let message = "Successfully delete selected category"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to delete selected category"
        error(res, message, err, 422)
    }
}
