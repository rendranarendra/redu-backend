const Tag = require('../models/tag.js')
const {
    success,
    error
} = require('../helpers/response.js')

exports.create = async (req, res) => {
    try{
        let result = await Tag.createTag(req.user.role, req.body)
        let message = "Successfully created tag"
        success(res, message, result, 201)
    }
    catch(err) {
        let message = "Failed to create tag"
        error(res, message, err, 422)
    }
}
exports.edit = async (req, res) => {
    try {
        let result = await Tag.updateTag(req.user.role, req.query.tagId, req.body)
        let message = "Successfully updated tag"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to update tag"
        error(res, message, err, 422)
    }
}

exports.purge = async (req, res) => {
    try {
        let result = await Tag.deleteTag(req.user.role, req.query.tagId)
        let message = "Successfully deleted tag"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to delete tag"
        error(res, message, err, 422)
    }
}

exports.view = async (req, res) => {
    await Tag.find({})
        .then(data => {
            let message = "Successfully show all tags"
            success(res, message, data, 200)
        })
}