const Comment = require('../models/comment.js')
const {
    success,
    error
} = require('../helpers/response.js')

exports.create = async (req, res) => {
    try {
        let result = await Comment.newComment(req.user._id, req.query.originId, req.body)
        let message = "Successfully created comment"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to create comment"
        error(res, message, err, 422)
    }
}

exports.mine = async (req, res) => {
    let result = await Comment.showMyComments(req.user._id, req.query.origin || undefined, req.query.page || 1, req.query.pagination || true)
    let message = "Successfully showed comments"
    success(res, message, result, 200)
}

exports.edit = async (req, res) => {
    try {
        let result = await Comment.editComment(req.user._id, req.query.commentId, req.body)
        let message = "Successfully edited comment"
        success(res, message, result, 201)
    }
    catch (err) {
        let message = "Failed to edit comment"
        error(res, message, err, 422)
    }
}

exports.purge = async (req, res) => {
    try{
        let result = await Comment.deleteComment(req.user._id, req.query.commentId)
        let message = "Successfully deleted comment"
        success(res, message, result, 200)
    }
    catch (err) {
        let message = "Failed to delete comment"
        error(res, message, err, 422)
    }
}
