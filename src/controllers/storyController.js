const Story = require("../models/story");
const { success, error } = require("../helpers/response");

exports.createStory = async (req, res) => {
  try {
    let data = {
      author: req.user._id,
      ...req.body,
      image: req.file
    }

    let result = await Story.add(data);
    let message = "Your story has been created";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Failed to create story";
    error(res, message, err, 422);
  }
};

exports.viewStory = async (req, res) => {
  try {
    let result = await Story.findStory(
      req.user._id,
      req.query.page,
      req.query.pagination || true,
      req.query.title || undefined,
      req.query.body || undefined,
      req.query.category || undefined,
      req.query.location || undefined
    );
    let message = "successfully get story";
    success(res, message, result, 200);
  } catch (err) {
    let message = "You have not created any story yet!";
    error(res, message, err, 422);
  }
};

exports.viewAllStory = async (req, res) => {
  try {
    let result = await Story.findAll(
      req.query.page || 1,
      req.query.pagination || true,
      req.query.title || undefined,
      req.query.body || undefined,
      req.query.category || undefined,
      req.query.location || undefined
    );
    let message = "successfully get all story";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Something's wrong with your input";
    error(res, message, err, 422);
  }
};

exports.sortMyStory = async (req, res) => {
  try {
    let result = await Story.mySort(
      req.user._id,
      req.query.order,
      req.query.sort,
      req.query.page
    );
    let message = "Successfully sort Story";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Failed to sort Story";
    error(res, message, err, 422);
  }
};

exports.editStory = async (req, res) => {
  try {
    let result = await Story.edit(
      req.user._id,
      req.query.id,
      req.body,
      req.file
    )
    let message = "Successfully edit Story";
    success(res, message, result, 201);
  } catch (err) {
    let message = "Failed to update story, please check your input data!";
    error(res, message, err, 422);
  }
};

exports.deleteStory = async (req, res) => {
  try {
    let result = await Story.destroy(req.user._id, req.query.id);
    let message = "Successfully delete story";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Story not found!";
    error(res, message, err, 422);
  }
};

exports.like = async (req, res) => {
  try {
    let result = await Story.likeStory(req.user._id, req.query.storyId);
    let message = "Successfully liked story"
    success(res, message, result, 201)
  }
  catch (err) {
    let message = "Failed to like story"
    error(res, message, err, 422)
  }
}

exports.filterStory = async (req, res) => {
  try {
    let result = await Story.filterCategory(
      req.query.category,
      req.query.page || 1
    );
    let message = "Successfully filter stories";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Invalid filter parameter";
    error(res, message, err, 422);
  }
};

exports.findOneStory = async (req, res) => {
  try {
    let result = await Story.findByID(
      req.query.id
    );
    let message = "Successfully get selected story";
    success(res, message, result, 200);
  } catch (err) {
    let message = "Story not found!";
    error(res, message, err, 404)
  }
}