const express = require('express')
const router = express.Router()

const user = require('./controllers/userController.js')
const story = require('./controllers/storyController.js')
const category = require('./controllers/categoryController.js')
const article = require('./controllers/articleController.js')
const comment = require('./controllers/commentController.js')
const tag = require('./controllers/tagController.js')

//Middlewares
const authenticate = require('./middlewares/authenticate.js')
const multer = require('./middlewares/multer.js')

//User router
router.post('/users', user.create)
router.post('/auth', user.auth)
router.get('/users', authenticate, user.current)
router.put('/users', multer, authenticate, user.update)
router.delete('/users', authenticate, user.deactivate)

//Admin router
router.post('/admin', user.admin)

//Stories router
router.post('/story', multer, authenticate, story.createStory)
router.post('/story/like', authenticate, story.like)
router.get('/story/views', authenticate, story.viewStory)
router.get('/story', authenticate, story.sortMyStory)
router.put('/story/update', multer, authenticate, story.editStory)
router.delete('/story/delete', authenticate, story.deleteStory)
router.get('/story/viewsAll', story.viewAllStory)
router.get('/story/filter', story.filterStory)
router.get('/story/get', story.findOneStory)

//Category router
router.post('/category/create', authenticate, category.createCategory)
router.get('/category', category.getAllCategory)
router.put('/category/update', authenticate, category.updateCategory)
router.delete('/category/delete', authenticate, category.deleteCategory)

//Article router
router.post('/articles', multer, authenticate, article.create)
router.post('/articles/like', authenticate, article.like)
router.get('/articles', article.view)
router.get('/articles/mine', authenticate, article.mine)
router.get('/articles/view', article.viewOne)
router.get('/articles/tag', article.tag)
router.put('/articles', multer, authenticate, article.edit)
router.delete('/articles', authenticate, article.purge)

//Comment router
router.post('/comments', authenticate, comment.create)
router.get('/comments/mine', authenticate, comment.mine)
router.put('/comments', authenticate, comment.edit)
router.delete('/comments', authenticate, comment.purge)

//Tag router
router.post('/tags', authenticate, tag.create)
router.get('/tags', tag.view)
router.put('/tags', authenticate, tag.edit)
router.delete('/tags', authenticate, tag.purge)

module.exports = router
